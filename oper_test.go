// oper test
// 可以在当前工作目录中输入以下命令执行测试:
// go test -v .
package oper

import (
	"fmt"
	"testing"
)

// 测试 Access()
func TestAccess(t *testing.T) {
	err := Access("https://golang.google.cn")
	if err != nil {
		t.Error(err)
	}
}

// 示例
func ExampleAccess() {
	err := Access("https://golang.google.cn")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(err)
	// Output: nil
}
